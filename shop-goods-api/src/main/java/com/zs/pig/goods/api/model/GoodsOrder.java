/** Powered By zscat科技, Since 2016 - 2020 */

package com.zs.pig.goods.api.model;

import javax.persistence.Table;

import com.zs.pig.common.base.BaseEntity;


/**
 * 
 * @author zsCat 2017-1-7 16:07:35
 * @Email: 951449465@qq.com
 * @version 1.0v
 *	商品订单管理
 */
@SuppressWarnings({ "unused"})
@Table(name="t_goodsorder")
public class GoodsOrder extends BaseEntity {

	private static final long serialVersionUID = 1L;

  		 private Long goodsid;
public Long getGoodsid() {return this.getLong("goodsid");}
public void setGoodsid(Long goodsid) {this.set("goodsid",goodsid);}
private Long orderid;
public Long getOrderid() {return this.getLong("orderid");}
public void setOrderid(Long orderid) {this.set("orderid",orderid);}


}
